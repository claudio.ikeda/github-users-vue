# github-users-vue

A simple Vue.js application to search users in github, exploring the Github API.


https://docs.github.com/en/rest/search#search-users


## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
