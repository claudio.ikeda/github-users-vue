import { parseError } from "@/utils/parseError";
import { convertToURLParams } from "@/utils/convertToURLParams";

export const get = async (url: string, params: any) => {
  const searchParams = convertToURLParams(params);

  return fetch(`${url}${searchParams}`)
    .then((response) => {
      if (!response.ok) {
        return parseError(response);
      }

      return response.json();
    })
    .catch((error) => parseError(error));
};
