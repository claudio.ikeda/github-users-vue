import { get } from "@/infra/http";

export const requestSearchUser = async (
  username: string,
  page: number,
  per: number
) => {
  const params = { q: username, per_page: per, page };

  try {
    return get("https://api.github.com/search/users", params);
  } catch (error) {
    console.log({ error });
  }
};
