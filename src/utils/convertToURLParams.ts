export const convertToURLParams = (params: any) => {
  if (!params) return "";
  return `?${new URLSearchParams(params)}`;
};
